// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  company: "Thành Bưởi",
  title: "Thành Bưởi",
  name: "Thành Bưởi",
  namePrint: "Thành Bưởi",

  gMapKey: "AIzaSyCkR-wl8rYwyb5XhvinbcfZOuFsRV2IFDo",
  tokenFirebase: "AIzaSyAY9k6gMHd_wx-IiLyhzzYdKh_xhpOxKeM",
  urlFirebase: "https://fcm.googleapis.com/fcm/send",

  urlLogo: "/assets/images/logo/logo.png",

  hub: {
    centerHubLongName: "",
    centerHubSortName: "",
    poHubLongName: "",
    poHubSortName: "",
    stationHubLongName: "",
    stationHubSortName: "",
  },

  formatDate: "YYYY/MM/DD",
  formatDateTable: "yyyy/MM/dd",
  formatDateTime: "YYYY/MM/DD HH:mm",
  formatDateTimeTable: "yyyy/MM/dd HH:mm",
// 
  // apiGeneralUrl:" http://api.anbms.name.vn/api",
  apiGeneralUrl:'http://103.149.28.153:8083/api',
  firebase: {
    apiKey: "AIzaSyBwjHPz6q5c0lNukX_9q_UXD3SiviB8cOU",
    authDomain: "dsc-demo-cb5fb.firebaseapp.com",
    databaseURL: "",
    projectId: "dsc-demo-cb5fb", 
    storageBucket: "dsc-demo-cb5fb.appspot.com",
    messagingSenderId: "631002885321"
  },
};

