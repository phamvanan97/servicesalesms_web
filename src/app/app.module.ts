import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule }    from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginModule } from './login/login.module';
import { BaseComponent } from './shared/base/base.component';
import { Page404Component } from './shared/page404/page404.component';
import { Page401Component } from './shared/page401/page401.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeadersInterceptor } from './shared/http-interceptor/headers.interceptor';
import { AppSecuredModule } from './app-secured/app-secured.module';
import { ToastModule } from 'primeng/toast';
import { NgProgressModule } from 'ngx-progressbar';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    Page404Component,
    Page401Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CalendarModule,
    CheckboxModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    NgProgressModule,
    AppSecuredModule,
    LoginModule,
    ToastModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HeadersInterceptor,
    multi: true,
}]
,
  bootstrap: [AppComponent]
})
export class AppModule { }
