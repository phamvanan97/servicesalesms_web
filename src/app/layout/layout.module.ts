import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CardComponent } from './card/card.component';
import {MenuModule} from 'primeng/menu';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MenuModule
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    CardComponent,
  ],
  exports:  [
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    CardComponent
  ],
  providers: [
    //PageService
  ]
})

export class LayoutModule { }
