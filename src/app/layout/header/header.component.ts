import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { AuthService } from 'src/app/services/auth.service';
import { MenuItem, MessageService } from 'primeng/api';
import { PermissionService } from 'src/app/services/permission.service';


@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styles: []
})

export class HeaderComponent extends BaseComponent implements OnInit {

  userFullName: string = "ADmin";
  phoneModal: BsModalRef = new BsModalRef;
  constructor(
    private authService: AuthService,
    private modalService: BsModalService,
    // private pageService: PageService,
    public override permissionService: PermissionService,
    public override router: Router,
    protected override messageService: MessageService,
  ) {
    super(messageService, permissionService, router);
  }

  items: MenuItem[] = [];
  override ngOnInit() {
    this.userFullName = this.authService.getFullName();
    this.items = [{
      label: 'Đổi mật khẩu',
      icon: 'fa fa-key',
      command: () => {
        this.changePass()
      }
    },
    {
      label: 'Đăng xuất',
      icon: 'fa fa-sign-out',
      command: () => {
        this.logOut();
      }
    }
    ];
  }

  public phoneNumber: string = "";
  account: any;
  listNotifications: any[] = []
  hasNewNotification: any = true;
  selectedShipmentType: any
  searchShipmentNumberGB: any = ""
  totalNotificationNew: any;
  isSeen: any;
  app: any
  public logOut() {
    this.authService.logout();
  }

  changePass() {
    this.router.navigate(["Changepass"]);
  }
  navigate() {

  }
  onClickBell() {
    // this.persistenceService.set("hasNewNotification", false, { type: StorageType.LOCAL });
    this.hasNewNotification = false;
  }
  onEnterSearchGB(textSearch: any) {

  }
  SeenNotification(data: any) {

  }
}
