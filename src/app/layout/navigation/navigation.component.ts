import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/models/page.model';
import { environment } from 'src/environments/environment';
declare var jQuery: any;

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: []
})
export class NavigationComponent implements OnInit {

  constructor() { }
  logo: string = environment.urlLogo;
  logoMini: any;
  pages: Page[] = [
    {
      id: 1,
      code: "BranchManagerment",
      name: "Quản lý đại lý con",
      concurrencyStamp: "123",
      isEnabled: true,
      createdBy:0,
      createdWhen:null,
      modifiedBy:0,
      modifiedWhen:null,
      parentPageId: 0, aliasPath: "BranchManagerment", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-user", display: "", children: [], rolePage: undefined, active: "", background: ""
    },
    {
      id: 1,
      code: "ServiceTypeManagerment",
      name: "Phân loại gói dịch vụ",
      concurrencyStamp: "123",
      isEnabled: true,
      createdBy:0,
      createdWhen:null,
      modifiedBy:0,
      modifiedWhen:null,
      parentPageId: 0, aliasPath: "/ServiceTypeManagerment", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-list-alt", display: "", children: [], rolePage: undefined, active: "", background: ""
    },
    {
      id: 2,
      code: "ServiceManager",
      name: "Danh sách gói dịch vụ",
      concurrencyStamp: "123",
      isEnabled: true,
      createdBy:0,
      createdWhen:null,
      modifiedBy:0,
      modifiedWhen:null,
      parentPageId: 0, aliasPath: "/ServiceManager", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-database", display: "", children: [], rolePage: undefined, active: "", background: ""
    },
    {
      id: 1,
      code: "InviteService",
      name: "Mời gói dịch vụ",
      concurrencyStamp: "123",
      isEnabled: true,
      createdBy:0,
      createdWhen:null,
      modifiedBy:0,
      modifiedWhen:null,
      parentPageId: 0, aliasPath: "/InviteService", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-paper-plane", display: "", children: [], rolePage: undefined, active: "", background: ""
    },
    {
      id: 1,
      code: "Notification",
      name: "Quản lý bản tin",
      concurrencyStamp: "123",
      isEnabled: true,
      createdBy:0,
      createdWhen:null,
      modifiedBy:0,
      modifiedWhen:null,
      parentPageId: 0, aliasPath: "/Notification", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-bell-o", display: "", children: [], rolePage: undefined, active: "", background: ""
    },
    {
      aliasPath: "/ReportManagement",
      code: "reportManagement",
      concurrencyStamp: "null",
      icon:"fa fa-area-chart",
      id: 1304,
      isAccess: true,
      isAdd: true,
      isDelete: true,
      isEdit: true,
      isEnabled: true,
      display: "",
      modulePageId: 0,
      name: "Thống kê doanh thu",
      pageOrder: 0,
      createdBy:0,
      createdWhen:null,
      modifiedBy:0,
      modifiedWhen:null,
      children: [
        {
          id: 2,
          code: "Revenue",
          name: "Doanh thu",
          concurrencyStamp: "123",
          isEnabled: true,
          createdBy:0,
          createdWhen:null,
          modifiedBy:0,
          modifiedWhen:null,
          parentPageId: 1, aliasPath: "/ReportManagement/Revenue", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-money", display: "", children: [], rolePage: undefined, active: "", background: ""
        },
        {
          id: 2,
          code: "Percentage",
          name: "Hoa hồng",
          concurrencyStamp: "123",
          isEnabled: true,
          createdBy:0,
          createdWhen:null,
          modifiedBy:0,
          modifiedWhen:null,
          parentPageId: 1, aliasPath: "/ReportManagement/Percentage", pageOrder: 1, isAccess: true, isAdd: true, isEdit: true, isDelete: true, modulePageId: 1, icon: "fa fa-usd", display: "", children: [], rolePage: undefined, active: "", background: ""
        },
      ],
      rolePage: undefined,
      active: "",
      background: "",
      parentPageId: 0
    }
  ];

  sideMenu: any;
  styleBackGround!: string;
  isToggle: boolean = false;
  saveSelectedIndex!: number;
  selectNavItemIndex!: number;
  ngOnInit(): void {
    this.pages.forEach(element => {
      element.display = "none";
      element.active = "noactive";
    });
  }
  ngAfterViewInit() {
    // jQuery("#side-menu").metisMenu();

    // if (jQuery("body").hasClass("fixed-sidebar")) {
    //   jQuery(".sidebar-collapse").slimscroll({
    //     height: "100%"
    //   });
    // }
  }
  selectNav(select: any) {
    if (
      !select.parentPageId &&
      (select.children && select.children.length === 0)
    ) {
      this.pages.forEach(x => {
        if (x != select) {
          let index = this.pages.indexOf(x);
          this.pages[index].display = "none";
          this.pages[index].active = "noactive";
        }
      });
      this.isToggle = false;
    }
    if (
      !select.parentPageId &&
      (select.children && select.children.length > 0)
    ) {
      let index = this.pages.indexOf(select);
      this.saveSelected();
      this.selectNavItemIndex = index;
    }
  }

  saveSelected() {
    this.saveSelectedIndex = this.selectNavItemIndex;
  }

  showItems(event: any, page: any) {
    setTimeout(() => {
      if (
        this.saveSelectedIndex == null ||
        this.saveSelectedIndex !== this.selectNavItemIndex
      ) {
        this.isToggle = true;
        this.pages[this.selectNavItemIndex].display = "block";
        this.pages[this.selectNavItemIndex].active = "active";
        if (this.saveSelectedIndex) {
          this.pages[this.saveSelectedIndex].display = "none";
          this.pages[this.saveSelectedIndex].active = "noactive";
        }
      } else {
        this.pages[this.selectNavItemIndex].display = "none";
        this.pages[this.selectNavItemIndex].active = "noactive";
        if (this.saveSelectedIndex) {
          this.pages[this.saveSelectedIndex].display = "none";
          this.pages[this.saveSelectedIndex].active = "noactive";
        }
        //
        this.isToggleItem();
      }
    }, 0);
  }

  isToggleItem() {
    if (this.isToggle == false) {
      this.pages[this.selectNavItemIndex].display = "block";
      this.pages[this.selectNavItemIndex].active = "active";
      this.isToggle = true;
    } else {
      this.pages[this.selectNavItemIndex].display = "none";
      this.pages[this.selectNavItemIndex].active = "noactive";
      this.isToggle = false;
    }
  }

}
