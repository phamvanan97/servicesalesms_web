export class RolePage{
    id!: number;
    roleId!: number;
    pageId!: number;
    isAccess: boolean = false;
    isAdd: boolean = false;
    isEdit: boolean = false;
    isDelete: boolean = false;
}