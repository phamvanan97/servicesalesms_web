export class GeneralModel{
    id: number=0;
    code: string="";
    name: string="";
    concurrencyStamp: string="";
    isEnabled: boolean = true;
    createdWhen:any= Date;
    createdBy:number = 0;
    modifiedWhen:any = Date;
    modifiedBy:number = 0;
}