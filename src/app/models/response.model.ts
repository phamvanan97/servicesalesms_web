export class ResponseModel{
    isSuccess: boolean = false;
    message!: string;
    data: any;
    dataCount: any;
    exception!: object;
}