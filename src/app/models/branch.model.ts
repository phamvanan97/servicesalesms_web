import { GeneralModel } from "./general.model";

export class Branch extends GeneralModel{
    phoneNumber?:string;
    address?:string;
    roleId?:string;
    password?:string;
    roleName?:string;
    fullName?:string;
}