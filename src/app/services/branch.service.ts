import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/response.model';
import { GeneralService } from './general.service';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BranchService extends GeneralService{

  constructor(
    private router: Router,
    protected override messageService: MessageService,
    protected override httpClient: HttpClient
  ) {
    super(httpClient,messageService,environment.apiGeneralUrl,"account");
  }
  async getListBranch(phoneNumber:any, name:any, pageNumber:any, pageSize:any):Promise<ResponseModel>
  {
    let params = new HttpParams();
    params = params.append("phoneNumber",phoneNumber)
    params = params.append("name",name)
    params = params.append("pageNumber",pageNumber)
    params = params.append("pageSize",pageSize)
    return await firstValueFrom(this.getCustomApi("GetListUser",params));
  }
}
