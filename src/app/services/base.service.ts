import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message, MessageService } from 'primeng/api';
import { firstValueFrom, Observable } from 'rxjs';
import { ResponseModel } from '../models/response.model';
import { Constant } from '../shared/infrastructures/constant';

// @Injectable({
//   providedIn: 'root'
// })
export class BaseService {

    constructor(
        protected httpClient: HttpClient,
        protected messageService: MessageService,
        protected urlName: string,
        protected apiName: string,
    ) { }
    public getCustomApi(apiMethod: string, params: HttpParams): Observable<ResponseModel> {
        return this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, { params: params });
    }
    public getApi(apiMethod: string, params: HttpParams): Promise<ResponseModel> {
        return firstValueFrom(this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, { params: params }));
    }

    public postCustomApi(apiMethod: string, model: Object): Observable<ResponseModel> {
        return this.httpClient.post<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, model);
    }
    async postApi(apiMethod: string, model: object): Promise<ResponseModel> {
        return firstValueFrom(this.httpClient.post<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, model));
    }

    public getCustomApiPaging(apiMethod: string, arrCols: string[] = [], params = new HttpParams(), pageSize?: number, pageNumber?: number): Observable<ResponseModel> {
        let cols = null;

        if (arrCols.length > 0) {
            cols = arrCols.join(',');
        }

        if (!params) params = new HttpParams();

        if (pageSize)
            params = params.append("pageSize", pageSize.toString());
        if (pageNumber)
            params = params.append("pageNumber", pageNumber.toString());
        if (cols)
            params = params.append("cols", cols);

        return this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, { params: params });
    }

    public postCustomUrlApi(urlName: string, apiMethod: string, model: Object): Observable<ResponseModel> {
        return this.httpClient.post<ResponseModel>(`${urlName}/${this.apiName}/${apiMethod}`, model);
    }

    public getCustomUrlApiPaging(urlName: string, apiMethod: string, arrCols: string[] = [], params = new HttpParams(), pageSize?: number, pageNumber?: number): Observable<ResponseModel> {
        let cols = null;

        if (arrCols.length > 0) {
            cols = arrCols.join(',');
        }

        if (!params) params = new HttpParams();

        if (pageSize)
            params = params.append("pageSize", pageSize.toString());
        if (pageNumber)
            params = params.append("pageNumber", pageNumber.toString());
        if (cols)
            params = params.append("cols", cols);

        return this.httpClient.get<ResponseModel>(`${urlName}/${this.apiName}/${apiMethod}`, { params: params });
    }
    isValidResponse(x: ResponseModel): boolean {
        if (!x.isSuccess) {
            if (x.message) {
                this.messageService.add({ severity: Constant.messageStatus.warn, detail: x.message });
            } else if (x.data) {
                let mess: Message[] = [];

                for (let key in x.data) {
                    let element = x.data[key];
                    mess.push({ severity: Constant.messageStatus.warn, detail: element });
                }

                this.messageService.addAll(mess);
            }
            else {
                this.messageService.add({ severity: Constant.messageStatus.error, detail: "Đã có lỗi xảy ra! Vui lòng thử lại!" });
            }
        }

        return x.isSuccess;
    }
}
