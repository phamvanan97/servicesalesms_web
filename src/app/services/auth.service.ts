import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Constant } from '../shared/infrastructures/constant';
import { BaseService } from './base.service';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/response.model';
import { Observable, firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  constructor(
    private router: Router,
    protected override messageService: MessageService,
    protected override httpClient: HttpClient
  ) {
    super(httpClient,messageService,environment.apiGeneralUrl,"Account");
  }
  isLogin:boolean = true;
  // localStorage = require('local-storage');
  isLogged() {
    let token = localStorage.getItem(Constant.auths.token);
    if (token) {
        return true;
    }
    return false;
  }

  // public isLoggedIn() : boolean {
  //   var isLogged = this.persistenceService.get(Constant.auths.isLoginIn, StorageType.LOCAL);
  //   if (isLogged == 'true') {
  //     return true;
  //   }
  //   return false;
  // }
  // Login(){
  //   this.isLogin = true;
  //   this.router.navigate([""]);
  // }
  async login(userName : string, password : string,hourLogin: number=0, minuteLogin: number=0) :Promise<ResponseModel> {
    let loginObj :any= new Object();
    loginObj["UserName"] = userName;
    loginObj["PassWord"] = password;
    loginObj["HourLogin"] = hourLogin;
    loginObj["MinuteLogin"] = minuteLogin;
    let data = null;
    const res = await firstValueFrom(this.postCustomApi("SignIn", loginObj))
    
    if (res.isSuccess == true) {
      this.messageService.add({severity:'success', summary:'Đăng nhập thành công', detail:'Đăng nhập thành công.'});
      var retData = res.data;
      data =res;
      localStorage.setItem(Constant.auths.token, retData["token"]);
      localStorage.setItem(Constant.auths.userId, retData["userId"]);  
      localStorage.setItem(Constant.auths.userName, retData["userName"]);
      localStorage.setItem(Constant.auths.fullName, retData["userFullName"]);
    } 
    return res;
  }

  public logout() {
    localStorage.clear();
    // var routing = Constant.pages.login.alias;
    this.isLogin = false;
    this.router.navigate(["login"]);
    // this.router.navigate([routing]);
  }

  public getToken(): string {
   const data = localStorage.getItem(Constant.auths.token);
    return data ??"";
  }

  public getUserId(): number {
    return Number(localStorage.getItem(Constant.auths.userId)??0);
  }

  public getUserName(): string {
    return localStorage.getItem(Constant.auths.userName)??"";
  }

  public getFullName(): string {
    return localStorage.getItem(Constant.auths.fullName)??"";
  }

  // logout(){
  //   this.isLogin = false;
  //   this.router.navigate(["login"]);
  // }
}
