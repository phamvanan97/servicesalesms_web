import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/response.model';
import { GeneralService } from './general.service';
import {BehaviorSubject, Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServiceTypeService extends GeneralService{

  constructor(
    private router: Router,
    protected override messageService: MessageService,
    protected override httpClient: HttpClient
  ) {
    super(httpClient,messageService,environment.apiGeneralUrl,"ServiceType");
  }

}
