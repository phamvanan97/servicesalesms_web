import { Inject, Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { ResponseModel } from '../models/response.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class GeneralService extends BaseService {

  constructor(
    protected override httpClient: HttpClient,
    protected override messageService: MessageService,
    @Inject('urlName') protected override urlName: string,
    @Inject('apiName') protected override apiName: string) {
    super(httpClient,messageService, urlName, apiName);
  }
  public get(id: any): Observable<ResponseModel> {
    return this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/get?id=${id}&cols=''`);
  }

  public getAll(arrCols: string[] = [], pageSize: number, pageNumber: number): Observable<ResponseModel> {
    let params = new HttpParams();

    if (!pageSize && !pageNumber && arrCols.length === 0) {
      return this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/getAll`);
    }
    else {
      let cols = null;

      if (arrCols.length > 0) {
        cols = arrCols.join(',');
      }

      if (pageSize)
        params = params.append("pageSize", pageSize.toString());
      if (pageNumber)
        params = params.append("pageNumber", pageNumber.toString());
      if (cols)
        params = params.append("cols", cols);

      return this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/getAll`, { params: params });
    }
  }

  public create(model: Object): Observable<ResponseModel> {
    return this.httpClient.post<ResponseModel>(`${this.urlName}/${this.apiName}/create`, model);
  }

  public update(model: Object): Observable<ResponseModel> {
    return this.httpClient.post<ResponseModel>(`${this.urlName}/${this.apiName}/update`, model);
  }

  public delete(model: Object): Observable<ResponseModel> {
    return this.httpClient.post<ResponseModel>(`${this.urlName}/${this.apiName}/delete`, model);
  }
}
