import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Constant } from 'src/app/shared/infrastructures/constant';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(
    private messageService:MessageService
  ) { }
  currentPassWord: string="";
  newPassWord: string="";
  reNewPassWord: string="";
  ngOnInit(): void {
  }
  changePassWord(): any {
    if (!this.isValid()) return;
    // return this.userService.changePassWord(this.currentPassWord, this.newPassWord).map(
    //   x => {
    //     if (!this.isValidResponse(x)) return;
    //     // this.authService.login(this.authService.getUserName(), this.newPassWord);
    //     this.authService.login(this.authService.getUserName(), this.newPassWord, this.hourLogin, this.minuteLogin);
    //     this.messageService.add({severity: Constant.messageStatus.success, detail: "Thay đổi mật khẩu thành công"});
    //     return true;
    //   }
    // );
  }

  changePassWordAfterLogin() {
    if (!this.isValid()) return;
    
    // this.userService.changePassWord(this.currentPassWord, this.newPassWord).subscribe(
    //   x => {
    //     if (!this.isValidResponse(x)) return;
    //     this.messageService.add({severity: Constant.messageStatus.success, detail: "Thay đổi mật khẩu thành công"});
    //   }
    // );
  }

  isValid(): boolean {
    if (!this.currentPassWord) {
      this.messageService.add({ severity: Constant.messageStatus.warn, detail: "Chưa nhập mật khẩu hiện tại" });
      return false;
    } else if (!this.newPassWord) {
      this.messageService.add({ severity: Constant.messageStatus.warn, detail: "Chưa nhập mật khẩu mới" });
      return false;
    } else if (!this.reNewPassWord) {
      this.messageService.add({ severity: Constant.messageStatus.warn, detail: "Chưa xác thực lại mật khẩu mới" });
      return false;
    } else if (this.newPassWord !== this.reNewPassWord) {
      this.messageService.add({ severity: Constant.messageStatus.warn, detail: "Mật khẩu xác thực không khớp" });
      return false;
    }

    return true;
  }
}
