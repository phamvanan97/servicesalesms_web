import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Dictionary, ComponentChild } from '@fullcalendar/angular/public_api';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RenderableProps } from 'preact';
import { MessageService, SelectItem } from 'primeng/api';
import { Branch } from 'src/app/models/branch.model';
import { ResponValidForm } from 'src/app/models/responValidate.model';
import { BranchService } from 'src/app/services/branch.service';
import { PermissionService } from 'src/app/services/permission.service';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Constant } from 'src/app/shared/infrastructures/constant';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.scss']
})
export class BranchComponent extends BaseComponent implements OnInit {
  parentPage: string = Constant.pages.branch.name;
  currentPage: string = Constant.pages.branch.name;
  formData!: FormGroup;
  constructor(
    private fb: FormBuilder,
    private bsModalService: BsModalService,
    private branchService: BranchService,
    protected override messageService: MessageService,
    protected override permissionService: PermissionService,
    protected override router: Router
  ) {
    super(messageService, permissionService, router);
    this.createForm();
  }
  val1?: string;
  listData: Branch[] = [];
  dataRow: Branch = new Branch();
  code: string = "";
  totalRecords: any = 0;
  pageNumber: number = 1;
  pageSize: number = 20;
  bsModalRef!: BsModalRef;
  branchs: SelectItem[] = []
  branchId?: number;
  roles: any[] = [
    { label: "Admin", value: 1 },
    { label: "Đại lý con", value: 2 }
  ];
  isEdit: boolean = false;
  phoneNumber: string = "";
  name: string = "";
  listDataShiff: any[] = []
  pageSizeModal: number = 10;
  totalModalRecords: number = 0;
  cols: SelectItem[] = [];
  override ngOnInit(): void {
    this.loadData();
  }

  createForm() {
    this.formData = this.fb.group({
      id: [this.dataRow?.id],
      fullName: [this.dataRow?.fullName || null, [Validators.required]],
      phoneNumber: [this.dataRow?.phoneNumber || null, [Validators.required, Validators.minLength(10), Validators.maxLength(11)]],
      password: ["", [this.isEdit ? Validators.required : Validators.nullValidator]],
      address: [this.dataRow?.address || "", [Validators.required, Validators.maxLength(100)]],
      roleId: [this.dataRow?.roleId || null, Validators.required]
    });
  }

  ngAfterViewInit() {
  }

  async loadData() {
    const res = await this.branchService.getListBranch(this.phoneNumber, this.name, this.pageNumber, this.pageSize);
    if (res) {
      this.listData = res.data
      this.totalRecords = res.dataCount;
    }
  }
  onPageChange(event: any) {
    this.pageNumber = event.first + 1;
    this.pageSize = event.rows
    this.loadData();
  }

  async openModelCreateUpdate(templeteCustomer: TemplateRef<any>, data?: Branch) {
    if (data) {
      this.formData.patchValue(data);
      this.branchService.get(data.id).subscribe({
        next: (res) => {

          this.dataRow = res.data;
        }
      });
      this.isEdit = true;
    }
    else {
      this.formData.reset();
      this.formData.controls['id'].setValue(0);
      this.isEdit = false;
    }
    this.bsModalRef = this.bsModalService.show(templeteCustomer, { class: "inmodal bounceInRight modal-md", backdrop: 'static', keyboard: true })
  }

  checkValidate(name: string): ResponValidForm {
    let res: ResponValidForm = { border: "error", messageError: "" };

    if (name == "name" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng nhập tên";
        return res;
      }
    }

    if (name == "phoneNumber" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng nhập số điện thoại";
        return res;
      }
      if (this.formData.controls[name].hasError('minlength')) {
        res.messageError = "Vui lòng nhập đủ số điện thoại";
        return res;
      }
      if (this.formData.controls[name].hasError('maxlength')) {
        res.messageError = "Vui lòng nhập đúng số điện thoại";
        return res;
      }
    }

    if (!this.isEdit && name == "password" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui nhập mật khẩu";
        return res;
      }
    }

    if (name == "address" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng nhập địa chỉ";
        return res;
      }
    }

    if (name == "roleId" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng chọn vai trò làm việc";
        return res;
      }
    }

    res.border = "";
    return res;
  }

  async save() {
    this.dataRow.fullName = this.formData.controls['fullName'].value;
    this.dataRow.phoneNumber = this.formData.controls['phoneNumber'].value;
    this.dataRow.password = this.formData.controls['password'].value;
    this.dataRow.address = this.formData.controls['address'].value;
    this.dataRow.roleId = this.formData.controls['roleId'].value;
    if (!this.isEdit) {
      this.dataRow.id = 0;
      this.branchService.create(this.dataRow).subscribe(res => {
        if (res.isSuccess == true) {
          this.messageService.add({ severity: 'success', summary: "success", detail: 'Thêm đại lý thành công' });
          this.loadData();
          this.bsModalRef.hide();
        }
        else {
          this.isValidResponse(res);
        }
      });
    }
    else {
      this.branchService.update(this.dataRow).subscribe(res => {
        if (res.isSuccess) {
          this.messageService.add({ severity: 'success', summary: "success", detail: 'Sửa đại lý thành công' });
          this.loadData();
          this.bsModalRef.hide();
        } else {
          this.isValidResponse(res);
        }
      });
    }
  }

  deleteUser(templeteCustomer: TemplateRef<any>, data?: Branch) {
    if (data)
      this.branchService.get(data.id).subscribe({
        next: (res) => {
          this.dataRow = res.data;
        }
      })
    this.bsModalRef = this.bsModalService.show(templeteCustomer, { class: "inmodal bounceInRight modal-sm", backdrop: 'static', keyboard: true })
  }

  confirmDelete() {
    if (this.dataRow) {
      const res = this.branchService.delete(this.dataRow).subscribe({
        next: () => {
          this.messageService.add({ severity: 'success', summary: "success", detail: 'Xóa nhân viên thành công' });
          this.loadData();
          this.bsModalRef.hide();
        }
      });
    }
  }

}
