import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportManagerRoutingModule } from './report-manager-routing.module';
import { RevenueComponent } from './revenue/revenue.component';
import { PercentageComponent } from './percentage/percentage.component';


@NgModule({
  declarations: [
    RevenueComponent,
    PercentageComponent
  ],
  imports: [
    CommonModule,
    ReportManagerRoutingModule
  ]
})
export class ReportManagerModule { }
