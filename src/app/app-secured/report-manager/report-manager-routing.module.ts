import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RevenueComponent } from './revenue/revenue.component';
import { PercentageComponent } from './percentage/percentage.component';

const routes: Routes = [
  { path: 'Revenue', component: RevenueComponent },
  { path: 'Percentage', component: PercentageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportManagerRoutingModule { }
