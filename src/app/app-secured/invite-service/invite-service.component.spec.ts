import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteServiceComponent } from './invite-service.component';

describe('InviteServiceComponent', () => {
  let component: InviteServiceComponent;
  let fixture: ComponentFixture<InviteServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InviteServiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InviteServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
