import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MessageService, SelectItem } from 'primeng/api';
import { Branch } from 'src/app/models/branch.model';
import { ResponValidForm } from 'src/app/models/responValidate.model';
import { ServiceTypeService } from 'src/app/services/service-type.service';
import { Constant } from 'src/app/shared/infrastructures/constant';
@Component({
  selector: 'app-service-type',
  templateUrl: './service-type.component.html',
  styleUrls: ['./service-type.component.scss']
})

export class ServiceTypeComponent implements OnInit {
  parentPage: string = Constant.pages.serviceType.name;
  currentPage: string = Constant.pages.serviceType.name;
  formData!: FormGroup;
  constructor(
    private fb: FormBuilder,
    private bsModalService : BsModalService,
    private serviceTypeService: ServiceTypeService,
    private messageService: MessageService
  ) { 
    this.createForm();
  }

  val1?:string;
  listData: Branch[] = [];
  dataRow!: Branch;
  code: string = "";
  totalRecords: any = 0;
  pageNumber: number = 1;
  pageSize: number = 20;
  bsModalRef!: BsModalRef;
  branchs: SelectItem[] = []
  branchId?: number;
  roles: any[] = [
    { label: "Admin", value: 1 },
    { label: "Phân loại gói dịch vụ", value: 2 }
  ];
  isEdit: boolean = false;
  phoneNumber: string = "";
  name: string = "";
  listDataShiff: any[] = []
  pageSizeModal: number = 10;
  totalModalRecords: number = 0;
  cols: SelectItem[] = [];

  ngOnInit(): void {
    this.loadData();
  }
  
  createForm() {
    this.formData = this.fb.group({
      id: [this.dataRow?.id],
      name: [this.dataRow?.name || null, [Validators.required]],
      phoneNumber: [this.dataRow?.phoneNumber || null, [Validators.required, Validators.minLength(10), Validators.maxLength(11)]],
      password: ["", [this.isEdit ? Validators.required : Validators.nullValidator]],
      address: [this.dataRow?.address || "", [Validators.required, Validators.maxLength(100)]],
      roleId: [this.dataRow?.roleId || null, Validators.required]
    });
  }

  async loadData() {
    this.serviceTypeService.getAll([], this.pageNumber, this.pageSize).subscribe(data => {
      console.log(data);
      if (data.isSuccess) {
        this.listData = data.data
        this.totalRecords = data.dataCount;
  
      }
    }
    );

  }

  onPageChange(event: any) {
    this.pageNumber = event.first + 1;
    this.pageSize = event.rows
    this.loadData();
  }

  async openModelCreateUpdate(templeteCustomer: TemplateRef<any>, data?: Branch) {
    if (data) {
      this.formData.patchValue(data);
      this.serviceTypeService.get(data.id).subscribe({
        next: (res) => {

          this.dataRow = res.data;
        }
      });
      this.isEdit = true;
    }
    else {
      this.formData.reset();
      this.formData.controls['id'].setValue(0);
      this.isEdit = false;
    }
    this.bsModalRef = this.bsModalService.show(templeteCustomer, { class: "inmodal bounceInRight modal-md", backdrop: 'static', keyboard: true })
  }
  checkValidate(name: string): ResponValidForm {
    let res: ResponValidForm = { border: "error", messageError: "" };

    if (name == "name" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng nhập tên";
        return res;
      }
    }

    if (name == "phoneNumber" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng nhập số điện thoại";
        return res;
      }
      if (this.formData.controls[name].hasError('minlength')) {
        res.messageError = "Vui lòng nhập đủ số điện thoại";
        return res;
      }
      if (this.formData.controls[name].hasError('maxlength')) {
        res.messageError = "Vui lòng nhập đúng số điện thoại";
        return res;
      }
    }

    if ( !this.isEdit && name == "password" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui nhập mật khẩu";
        return res;
      }
    }

    if (name == "address" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng nhập địa chỉ";
        return res;
      }
    }

    if (name == "roleId" && this.formData.controls[name]?.touched) {
      if (this.formData.controls[name].hasError('required')) {
        res.messageError = "Vui lòng chọn vai trò làm việc";
        return res;
      }
    }

    res.border = "";
    return res;
  }

  async save() {
    if (!this.isEdit) {
      this.serviceTypeService.create(this.formData.value).subscribe(res => {
        if (res.isSuccess) {
          this.messageService.add({ severity: 'success', summary: "success", detail: 'Thêm khách hàng thành công' });
          this.loadData();
          this.bsModalRef.hide();
        }
        else {
          this.serviceTypeService.isValidResponse(res);
        }
      });
    }
    else {
      this.dataRow.name = this.formData.controls['name'].value;
      this.dataRow.phoneNumber = this.formData.controls['phoneNumber'].value;
      this.dataRow.password = this.formData.controls['password'].value;
      this.dataRow.address = this.formData.controls['address'].value;
      this.dataRow.roleId = this.formData.controls['roleId'].value;
      this.serviceTypeService.update(this.dataRow).subscribe(res => {
        if (res.isSuccess) {
          this.messageService.add({ severity: 'success', summary: "success", detail: 'Sửa khách hàng thành công' });
          this.loadData();
          this.bsModalRef.hide();
        } else {
          this.serviceTypeService.isValidResponse(res);
        }
      });
    }
  }

  deleteUser(templeteCustomer: TemplateRef<any>, data?: Branch) {
    if (data)
      // this.employeeService.get(data.id).subscribe({
      //   next: (res) => {
      //     this.dataRow = res.data;
      //   }
      // })
    this.bsModalRef = this.bsModalService.show(templeteCustomer, { class: "inmodal bounceInRight modal-sm", backdrop: 'static', keyboard: true })
  }

  confirmDelete() {
    if (this.dataRow) {
      // const res = this.employeeService.delete(this.dataRow).subscribe({
      //   next: () => {
      //     this.messageService.add({ severity: 'success', summary: "success", detail: 'Xóa nhân viên thành công' });
      //     this.loadData();
      //     this.bsModalRef.hide();
      //   }
      // });
    }
  }
}
