import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/gaurd/auth.gaurd';
import { AppSecuredComponent } from './app-secured.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { BranchComponent } from './branch/branch.component';
import { ServiceTypeComponent } from './service-type/service-type.component';
import { ServiceManagerComponent } from './service-manager/service-manager.component';
import { InviteServiceComponent } from './invite-service/invite-service.component';
import { NotificationComponent } from './notification/notification.component';

const routes: Routes = [
  {
    path: '', component: AppSecuredComponent, children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'Changepass', component: ChangePasswordComponent },
      { path: 'BranchManagerment', component: BranchComponent },
      { path: 'ServiceTypeManagerment', component: ServiceTypeComponent },
      { path: 'ServiceManager', component: ServiceManagerComponent },
      { path: 'InviteService', component: InviteServiceComponent },
      { path: 'Notification', component: NotificationComponent },
      { path: 'ReportManagement', loadChildren: () => import('./report-manager/report-manager.module').then(m => m.ReportManagerModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppSecuredRoutingModule { }
