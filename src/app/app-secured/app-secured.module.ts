import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppSecuredRoutingModule } from './app-secured-routing.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { LayoutModule } from '../layout/layout.module';
import { AppSecuredComponent } from './app-secured.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageService } from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import { FormsModule } from '@angular/forms';
import { BranchComponent } from './branch/branch.component';
import { ServiceTypeComponent } from './service-type/service-type.component';
import { ServiceManagerComponent } from './service-manager/service-manager.component';
import { InviteServiceComponent } from './invite-service/invite-service.component';
import { NotificationComponent } from './notification/notification.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ChangePasswordComponent,
    AppSecuredComponent,
    BranchComponent,
    ServiceTypeComponent,
    ServiceManagerComponent,
    InviteServiceComponent,
    NotificationComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    SharedModule,
    AppSecuredRoutingModule,
    ToastModule,
    FormsModule
  ],
  providers:[
    BsModalService,
    MessageService
  ]
})
export class AppSecuredModule { }
