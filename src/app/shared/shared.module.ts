import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableModule} from 'primeng/table';
import { LayoutModule } from '../layout/layout.module';
import {TabViewModule} from 'primeng/tabview';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import {CheckboxModule} from 'primeng/checkbox';
import {InputMaskModule} from 'primeng/inputmask';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TableModule,
    LayoutModule,
    TabViewModule,
    FormsModule,
    DropdownModule,
    CalendarModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    ToastModule,
    CheckboxModule,
    InputMaskModule
  ],
  providers:[
    MessageService
  ],
  exports: [
    TableModule,
    LayoutModule,
    FormsModule,
    DropdownModule,
    CalendarModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    ToastModule,
    CheckboxModule,
    InputMaskModule
  ]
})
export class SharedModule { }
