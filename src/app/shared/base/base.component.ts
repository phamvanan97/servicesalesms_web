import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { ResponseModel } from 'src/app/models/response.model';
import { PermissionService } from 'src/app/services/permission.service';
import { Constant } from '../infrastructures/constant';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor(
    protected messageService: MessageService,
    protected permissionService: PermissionService,
    protected router: Router
  ) { }

  ngOnInit(): void {
  }

  isValidResponse(x: ResponseModel): boolean {
    if (!x.isSuccess) {
      if (x.message) {
        this.messageService.add({ severity: Constant.messageStatus.warn, detail: x.message });
      } else if (x.data) {
        let mess: Message[] = [];

        if (Array.isArray(x.data)) {
          x.data = x.data[0];
        }

        for (let key in x.data) {
          let element = x.data;
          if (key != 'key') {
            mess.push({ severity: Constant.messageStatus.warn, detail: element[key] });
          }
        }

        // result.forEach(element => {
        //   if (element.value) {
        //     mess.push({ severity: Constant.messageStatus.warn, detail: element.value });
        //   } else {
        //     mess.push({ severity: Constant.messageStatus.warn, detail: element.key });
        //   }
        // });

        this.messageService.addAll(mess);
      }
    }

    return x.isSuccess;
  }
}
