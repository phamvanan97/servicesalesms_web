export const Constant = {
    messageStatus: {
        success: "success",
        info: "info",
        warn: "warn",
        error: "warn",
    },
    response: {
        isSuccess: "isSuccess",
        message: "message",
        data: "data",
        exception: "exception",
    },
    auths: {
        isLoginIn: 'loggedIn',
        token: 'token',
        userId: 'userId',
        userName: 'userName',
        currentUser: 'currentUser',
        fullName: 'fullName',
        tokenFirebase: 'tokenFirebase',
        datetimeLogin: 'datetimeLogin',
        expires: 'expires',
        staticMenuActive: 'staticMenuActive'
    },
    userType: {
        ADMIN: 1,
        NHA_CUNG_CAP: 2,
        NGUOI_SU_DUNG: 3
    },
    supportsStatus: {
        MOI: 1,
        DANG_XU_LY: 2,
        CHUYEN_TIEP: 3,
        DA_XU_LY: 4
    },
    pages: {
        login: {
            name: 'Đăng nhập',
            alias: 'dang-nhap',
        },
        page404: {
            name: 'Không tìm thấy trang',
            alias: '404',
        },
        page403: {
            name: 'Không có quyền truy cập',
            alias: '403',
        },
        changePassWord: {
            name: 'Thay đổi mật khẩu',
            alias: 'thay-doi-mat-khau',
        },
       
        branch: {
            name: 'Quản lý đại lý con',
            alias: 'branch',
        },
        inviteService: {
            name: 'Mời gói dịch vụ',
            alias: 'inviteService',
        },
        notification: {
            name: 'Quản lý bản tin',
            alias: 'notification',
        },
        listService: {
            name: 'Danh sách gói dịch vụ',
            alias: 'listService',
        },
        serviceType: {
            name: 'Phân loại gói dịch vụ',
            alias: 'serviceType',
        },
        reportManager: {
            name: 'Thống kê doanh thu',
            alias: 'thong-ke-doanh thu',
            loadChildren: './report-manager/report-manager.module#ReportManagerModule',
            childrens: {
                revenue: {
                    name: 'Doanh thu',
                    alias: 'revenue',
                },
                percentage: {
                    name: 'Hoa hồng',
                    alias: 'percentage',
                },
            }
        },
    }
}