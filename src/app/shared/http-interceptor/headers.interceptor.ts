import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Constant } from '../infrastructures/constant';
import { LocalstoreService } from 'src/app/services/localstore.service';
import { MsgService } from 'src/app/services/msg.service';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private msgService: MsgService,
    private storageService: LocalstoreService
  ) { }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    var token = this.storageService.get(Constant.auths.token);
    var tokenFirebase = this.storageService.get(Constant.auths.tokenFirebase);
    var modReq = null;

    if (req.url == "https://fcm.googleapis.com/fcm/send") {
      const authHeader = "key=" + tokenFirebase;
      var updUrl = { url: req.url, headers: req.headers.set('Authorization', authHeader) };
      modReq = req.clone(updUrl);
    }
    else if (token) {
      const authHeader = "Bearer " + token;
      var updUrl = { url: req.url, headers: req.headers.set('Authorization', authHeader) };
      modReq = req.clone(updUrl);
    } else {
      var url = { url: req.url };
      modReq = req.clone(url);
    }
    return next.handle(modReq).pipe(map(event => {
      if (event instanceof HttpResponse) {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
        }
      }
      return event;
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // redirect to the login route
          // or show a modal
          this.storageService.removeAll();
          this.router.navigate(['/']);
          this.msgService.warn("Hết phiên làm việc");
        }
        if (err.status === 403) {
          // redirect to the login route
          // or show a modal
          // console.log("403");
          this.router.navigate(['/403']);
        }
      }
    }));
  }
}
