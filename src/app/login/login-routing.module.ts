import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLoginGuard } from '../shared/gaurd/authlogin.gaurd';
import { LoginComponent } from './login.component';

const routes: Routes = [
  {path:'',component:LoginComponent,canActivateChild:[AuthLoginGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
