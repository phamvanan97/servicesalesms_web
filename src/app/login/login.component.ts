import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private authService:AuthService,
    private messageService : MessageService
  ) { }
  username:string="";
   password : string="";
  //  evTitle: string;
   evName: string="";
  //  evCompany: string;
  //  hourLogin: number;
  //  minuteLogin: number;
   evLogo: string="https://play-lh.googleusercontent.com/ahJtMe0vfOlAu1XJVQ6rcaGrQBgtrEZQefHy7SXB7jpijKhu1Kkox90XDuH8RmcBOXNn";

  ngOnInit(): void {
  }

  async login(){
    const res = await this.authService.login(this.username,this.password);
    if(res.isSuccess){
      this.router.navigate([""]);
    }
    else{ 
      this.messageService.add({severity:'warn', summary:'Đăng nhập không thành công', detail: res.message});
    }
  }

  keyDownFunction(event:any){

  }

}
