import { NgModule, ViewChild } from '@angular/core';
import { NoPreloading, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/gaurd/auth.gaurd';
import { AuthLoginGuard } from './shared/gaurd/authlogin.gaurd';

const routes: Routes = [
  {path:'' ,loadChildren:()=> import('./app-secured/app-secured.module').then(m=>m.AppSecuredModule),canActivate:[AuthGuard]},
  {path:'login' ,loadChildren:()=> import('./login/login.module').then(m=>m.LoginModule), canActivate:[AuthLoginGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top', preloadingStrategy: NoPreloading })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
